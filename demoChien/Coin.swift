//
//  Coin.swift
//  demoChien
//
//  Created by tùng on 4/7/22.
//

import Foundation
import UIKit

class Coin{
    var id:Int = 0
    var code:String = ""
    var quantity:Double = 0
    var name:String = ""
    var lastPrice:Double = 0
    var usdBalance:Double = 0
    
    var image:String = ""
  
    func initLoad(_ json: [String: Any]) -> Coin{
        if let temp = json["id"] as? Int { id = temp}
        if let temp = json["code"] as? String { code = temp}
        if let temp = json["quantity"] as? Double { quantity = temp}
        if let temp = json["name"] as? String { name = temp}
        if let temp = json["last_price"] as? Double { lastPrice = temp}
        if let temp = json["usd_balance"] as? Double { usdBalance = temp}
        if let temp = json["url_icon"] as? String { image = temp}
        return self
    }
    
}
