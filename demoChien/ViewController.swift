//
//  ViewController.swift
//  demoChien
//
//  Created by tùng on 4/7/22.
//

import UIKit
import Alamofire
import Kingfisher

class ViewController: UIViewController {
    
    var coin:[Coin] = []
    var coin2:[Coin2] = []
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        let layout = UICollectionViewFlowLayout()
        collectionView.collectionViewLayout = layout
        
        requestApi()
       

        
        AF.request("https://private-dae7a5-nguyentienhung.apiary-mock.com/user/my_coins?fbclid=IwAR2k_qQkffME4S8wmg-YZm_67p0BkwUQPqAAXgsTi6IayvyAsxwxVlenMW8").responseData { (response) in
            switch response.result {
            case .success(let wallet):
                do {
                    let asJSON = try JSONSerialization.jsonObject(with: wallet)
                    if let data = asJSON as? [String : Any] {
                        if let result = data["data"] as? [String: Any]{
                            if let mycoin = result["my_coins"] as? [[String :Any]] {
                                for i in mycoin {
                                    var itemAdd = Coin()
                                    itemAdd = itemAdd.initLoad(i)
                                    self.coin.append(itemAdd)
                                    
                                    DispatchQueue.main.async {
                                        self.collectionView.reloadData()
                                    }
                                }
                            }
                        }
                        
                    }
                } catch {
                    print("error")
                }
            case .failure(let afError):
                print("Error : \(afError)")
            }
        }
        
    }
    
    func requestApi() {
        AF.request("https://private-dae7a5-nguyentienhung.apiary-mock.com/manager/list?type=all&fbclid=IwAR1iT7Hi3X9zuFaMo5dPKb3MOQLK33gndieXxpj0vkORW9JOcvS8JKljyYs").responseData { (response) in
            switch response.result {
            case .success(let wallet):
                do {
                    let asJSON = try JSONSerialization.jsonObject(with: wallet)
                    if let data = asJSON as? [String : Any] {
                        if let result = data["data"] as? [[String: Any]]{
                            print("result \(result)")
                            for i in result{
                                var itemAddData = Coin2()
                                itemAddData = itemAddData.initLoad(i)
                                self.coin2.append(itemAddData)
                                
                                DispatchQueue.main.async {
                                    self.collectionView.reloadData()
                                    
                                }
                            }
                            
                        }
                        
                    }
                    
                } catch {
                    print("error")
                }
            case .failure(let afError):
                print("Error : \(afError)")
            }
        }
    }
}

// Do any additional setup after loading the view.




extension ViewController:UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = seconvc(nibName: "seconvc", bundle: nil)
         self.navigationController!.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return coin2.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LabelCollectionViewCell", for: indexPath) as! LabelCollectionViewCell
        cell.setUp(with: coin2[indexPath.row])
        
        return cell
    }
}
extension ViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 300)
    }
}


