//
//  CoinApi2.swift
//  demoChien
//
//  Created by tùng on 4/7/22.
//

import Foundation
import UIKit

class Coin2 {
    var id:Int = 0
    var code:String = ""
    var name:String = ""
    var ischosse:Bool = true
    var image:String = ""
    
    func initLoad(_ json: [String: Any]) -> Coin2{
        if let temp = json["id"] as? Int { id = temp}
        if let temp = json["code"] as? String { code = temp}
        if let temp = json["name"] as? String { name = temp}
        if let temp = json["is_choose"] as? Bool {ischosse = temp}
        if let temp = json["url_icon"] as? String { image = temp}
        return self
    }
}
