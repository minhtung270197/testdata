//
//  LabelCollectionViewCell.swift
//  demoChien
//
//  Created by tùng on 4/7/22.
//

import UIKit
import Kingfisher

class LabelCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelView1: UILabel!
    
    
    @IBOutlet weak var labelView2: UILabel!
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    func setUp(with coin2:Coin2){
        labelView1.text = coin2.name
        labelView2.text = coin2.code
        imageView.kf.setImage(with: URL(string: coin2.image))
    }
}
